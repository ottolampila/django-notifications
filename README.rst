====================
django-notifications
====================

Simple notifications for Django


Installation
============

1. Make sure  ``django.contrib.contenttypes`` is in ``INSTALLED_APPS``.
2. Add ``notifications`` to ``INSTALLED_APPS``
3. Run ``manage.py migrate``


Features
========
- Simple to use
- Flexible
- Notifications are easy to translate


Example of usage
================

Create a notification

.. code-block:: python

    from django.contrib.auth.models import User
    from notifications.utils import send_notification


    recipient = User.objects.get(id=1)

    notification = send_notification(recipient, key='demo', context_object=recipient, extra_context={'foo': 'bar'})



Now to render this, create a template with the corresponding key to the ``notifications/`` template directory.
Since we use "demo" as the notification *key*, we create a notification template ``notifications/demo.html`` which looks like this:

.. code-block:: html

    {% load i18n %}
    <p class="notification">
        Greetings <a href="{% url 'users:detail' object.id %}">({{ object.name }})</a>
        <span>{{ foo }}</span>
    </p>


When we render this using ``Notification.render()``, we get something like

.. code-block:: html

    <p class="notification">
        Greetings <a href="/users/1/">My User</a>
        <span>bar</span>
    </p>
