# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from copy import copy

from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.template import loader
from django.utils.safestring import mark_safe


class NotificationManager(models.Manager):
    def unread(self):
        return self.get_queryset().filter(is_read=False)


class Notification(models.Model):
    key = models.CharField(max_length=50)
    recipient = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
        related_name='notifications')
    created_at = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)

    context_object_content_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE, null=True, blank=True,
        related_name='notification_context_object')
    context_object_id = models.PositiveIntegerField(null=True, blank=True)
    context_object = GenericForeignKey(
        'context_object_content_type', 'context_object_id')

    sender_content_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE, null=True, blank=True,
        related_name='notification_sender')
    sender_id = models.PositiveIntegerField(null=True, blank=True)
    sender_object = GenericForeignKey('sender_content_type', 'sender_id')

    extra_context = JSONField(null=True, blank=True)

    objects = NotificationManager()

    class Meta:
        ordering = ('is_read', '-created_at')

    def __unicode__(self):
        return '{} to {}'.format(self.key, self.recipient)

    def mark_as_read(self):
        """ Mark notification as read """
        if not self.is_read:
            self.is_read = True
            self.save()

    def mark_as_unread(self):
        """ Mark notification as unread """
        if self.is_read:
            self.is_read = False
            self.save()

    def get_context(self):
        """ Get context dict for the template to render """
        ctx = dict(copy(self.extra_context))
        ctx.update({
            'object': self.context_object,
            'sender': self.sender_object,
            'is_read': self.is_read,
            'created_at': self.created_at,
        })
        return ctx

    def get_template_name(self):
        """ Get the name of the template to render"""
        return 'notifications/{}.html'.format(self.key)

    def render(self):
        """ Render correct template using correct context """
        template = loader.get_template(self.get_template_name())
        return mark_safe(template.render(self.get_context()))
