# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.views.generic import ListView, TemplateView

from .models import Notification


class NotificationListView(LoginRequiredMixin, ListView):
    """ List notifications for request.user """
    model = Notification
    context_object_name = 'notifications'
    paginate_by = 10

    def get_queryset(self):
        return super(NotificationListView, self).get_queryset().filter(
            recipient=self.request.user
        )


class UnreadNotificationCountView(TemplateView):
    def render_to_response(self, context, **response_kwargs):
        if self.request.user.is_authenticated:
            result = self.request.user.notifications.unread().count()
        else:
            result = 0
        data = {'result': result}
        return JsonResponse(data, **response_kwargs)
