# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging

from django.contrib.contenttypes.models import ContentType
from django.db.models import Model

from .models import Notification

logger = logging.getLogger(__name__)


def send_notification(to, key='default', context_object=None,
                      sender=None, extra_context=None):
    data = dict(key=key, recipient=to)
    if extra_context is None:
        extra_context = {}
    data.update({'extra_context': extra_context})
    if context_object:
        assert isinstance(context_object, Model)
        content_type = ContentType.objects.get_for_model(context_object)
        data.update({
            'context_object_content_type': content_type,
            'context_object_id': context_object.pk
        })
    if sender:
        assert isinstance(sender, Model)
        content_type = ContentType.objects.get_for_model(sender)
        data.update({
            'sender_content_type': content_type,
            'sender_id': sender.pk
        })

    notification = Notification.objects.create(**data)
    return notification

