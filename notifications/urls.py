# -*- coding: utf-8 -*-
from django.conf.urls import url

from .views import NotificationListView, UnreadNotificationCountView


app_name = 'notifications'

urlpatterns = [
    url(r'^$', NotificationListView.as_view(), name='list'),
    url(r'^unread-count/$', UnreadNotificationCountView.as_view(),
        name='unread-count')
]
