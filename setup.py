# -*- coding: utf-8 -*-

from setuptools import setup

setup(name='notifications',
      version='0.1',
      description='Description of the package',
      url='https://bitbucket.org/ottolampila/django-notifications',
      author='Otto Lampila',
      author_email='otto.lampila@gmail.com',
      license='UNLICENSED',
      packages=['notifications'],
      zip_safe=False)
